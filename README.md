# Blogy

## Loading 
The following script installs Blogy in Pharo.

```smalltalk
Metacello new
  repository: 'gitlab://ortegacmanuel/blogy';
  baseline: 'Blogy';
  load
```
