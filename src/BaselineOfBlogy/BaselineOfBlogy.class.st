Class {
	#name : #BaselineOfBlogy,
	#superclass : #BaselineOf,
	#category : #BaselineOfBlogy
}

{ #category : #baselines }
BaselineOfBlogy >> baseline:	 spec [
	<baseline>	

	spec for: #pharo do: [		
		spec package: 'Blogy'.
		spec package: 'Blogy-Tests' with: [ spec requires: #('Blogy') ]
	]
]
