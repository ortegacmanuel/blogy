Class {
	#name : #BYBlogTest,
	#superclass : #TestCase,
	#instVars : [
		'blog',
		'post',
		'first',
		'previousRepository'
	],
	#category : #'Blogy-Tests'
}

{ #category : #initialization }
BYBlogTest >> setUp [
	
	previousRepository := VORepository current.
	VORepository setRepository: VOMemoryRepository new.
			
	blog := 	BYBlog current.
	blog removeAllPosts.
		
	first := 	BYPost title: 'A title' text: 'A text' category: 'First Category'.
	blog writeBlogPost: first.
	
	post :=	(BYPost title: 'Another title' text: 'Another text'
category: 'Second Category') beVisible
]

{ #category : #initialization }
BYBlogTest >> tearDown [
	
	VORepository setRepository: previousRepository
]

{ #category : #tests }
BYBlogTest >> testAddBlogPost [
	
	blog writeBlogPost: post.
	self assert: blog 	size equals: 2
]

{ #category : #tests }
BYBlogTest >> testAllBlogPosts [
		
		"blog writeBlogPost: post."
		self assert: blog allBlogPosts size equals: 1
]

{ #category : #tests }
BYBlogTest >> testAllBlogPostsFromCategory [
	
	self assert: (blog allBlogPostsFromCategory: 'First Category') size equals: 1
]

{ #category : #tests }
BYBlogTest >> testAllCategories [
	
	blog writeBlogPost: post.
	self assert: blog allCategories size equals: 2
]

{ #category : #tests }
BYBlogTest >> testAllVisibleBlogPosts [
	
	blog writeBlogPost: post.
	self assert: blog allVisibleBlogPosts size equals: 1
]

{ #category : #tests }
BYBlogTest >> testAllVisibleBlogPostsFromCategory [
	
	blog writeBlogPost: post.
	self assert: (blog allVisibleBlogPostsFromCategory: 'First Category') size equals: 0.
	self assert: (blog allVisibleBlogPostsFromCategory: 'Second Category') size equals: 1
]

{ #category : #tests }
BYBlogTest >> testRemoveAllBlogPosts [
	
	blog removeAllPosts.
	self assert: blog size equals: 0
]

{ #category : #tests }
BYBlogTest >> testRemoveBlogPost [

	self assert: blog size equals: 1.
	blog removeBlogPost: blog allBlogPosts anyOne.
	self assert: blog size equals: 0
]

{ #category : #tests }
BYBlogTest >> testSize [
	
	self assert: blog size equals: 1
]

{ #category : #tests }
BYBlogTest >> testUnclassifiedBlogPosts [
	
	self assert: (blog allBlogPosts select: [ :p | p isUnclassified]) size equals: 0
]
