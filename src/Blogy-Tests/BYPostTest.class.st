Class {
	#name : #BYPostTest,
	#superclass : #TestCase,
	#category : #'Blogy-Tests'
}

{ #category : #tests }
BYPostTest >> testPostIsCreatedCorrectly [

	| post |
	post := BYPost
		        title: 'Welcome to TinyBlog'
		        text: 'Blogy is a small blog engine made with Pharo.'
				  category: 'Pharo'.

	self assert: post title equals: 'Welcome to TinyBlog'.
	self assert: post category equals: 'Pharo'
]

{ #category : #tests }
BYPostTest >> testWithoutCategoryIsUnclassified [

	| post |
	post := BYPost
		        title: 'Welcome to TinyBlog'
		        text: 'Blogy is a small blog engine made with Pharo.'.

	self assert: post title equals: 'Welcome to TinyBlog'.
	self assert: post isUnclassified.
	self deny: post isVisible
]
