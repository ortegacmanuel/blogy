Class {
	#name : #BYPost,
	#superclass : #Object,
	#instVars : [
		'title',
		'text',
		'date',
		'category',
		'visible'
	],
	#category : #'Blogy-Domain'
}

{ #category : #'instance creation' }
BYPost class >> title: aTitle text: aText [
	^ self new
		title: aTitle;
		text: aText;
		yourself
			 
]

{ #category : #'instance creation' }
BYPost class >> title: aTitle text: aText category: aCategory [
	^ (self title: aTitle text: aText)
		category: aCategory;
		yourself
			 
]

{ #category : #'as yet unclassified' }
BYPost class >> unclassifiedTag [
	^ 'Unclassified'
]

{ #category : #action }
BYPost >> beVisible [

	self visible: true
]

{ #category : #accessing }
BYPost >> category [

	^ category
]

{ #category : #accessing }
BYPost >> category: anObject [

	category := anObject
]

{ #category : #accessing }
BYPost >> date [

	^ date
]

{ #category : #accessing }
BYPost >> date: aDate [

	date := aDate
]

{ #category : #'magritte-descriptions' }
BYPost >> descriptionCategory [

	<magritteDescription>
	^ MAStringDescription new
		label: 'Category';
		priority: 300;
		accessor: #category;
		yourself
]

{ #category : #'magritte-descriptions' }
BYPost >> descriptionDate [

	<magritteDescription>
	^ MADateDescription new
		label: 'Date';
		priority: 400;
		accessor: #date;
		beRequired;
		yourself
]

{ #category : #'magritte-descriptions' }
BYPost >> descriptionText [

	<magritteDescription>
	^ MAMemoDescription new
		label: 'Text';
		priority: 200;
		accessor: #text;
		beRequired;
		yourself
]

{ #category : #'magritte-descriptions' }
BYPost >> descriptionTitle [

	<magritteDescription>
	^ MAStringDescription new
		label: 'Title';
		priority: 100;
		accessor: #title;
		beRequired;
		yourself
]

{ #category : #'magritte-descriptions' }
BYPost >> descriptionVisible [

	<magritteDescription>
	^ MABooleanDescription new
		label: 'Visible';
		priority: 500;
		accessor: #visible;
		beRequired;
		yourself
]

{ #category : #accessing }
BYPost >> initialize [
	super initialize.
	self category: BYPost unclassifiedTag.
	self date: Date today.	
	self notVisible
]

{ #category : #testing }
BYPost >> isUnclassified [
	^ self category = BYPost unclassifiedTag
]

{ #category : #testing }
BYPost >> isVisible [
	^ self visible
]

{ #category : #action }
BYPost >> notVisible [

	self visible: false
]

{ #category : #accessing }
BYPost >> text [

	^ text
]

{ #category : #accessing }
BYPost >> text: aString [

	text := aString
]

{ #category : #accessing }
BYPost >> title [

	^ title
]

{ #category : #accessing }
BYPost >> title: aString [

	title := aString
]

{ #category : #accessing }
BYPost >> visible [

	^ visible
]

{ #category : #accessing }
BYPost >> visible: aBoolean [

	visible := aBoolean
]
