Class {
	#name : #BYSession,
	#superclass : #WASession,
	#instVars : [
		'currentAdmin'
	],
	#category : #'Blogy-Components'
}

{ #category : #accessing }
BYSession >> currentAdmin [

	^ currentAdmin
]

{ #category : #accessing }
BYSession >> currentAdmin: anObject [

	currentAdmin := anObject
]

{ #category : #accessing }
BYSession >> isLogged [

	^ self currentAdmin notNil
]

{ #category : #initialization }
BYSession >> reset [

	currentAdmin := nil.
	self requestContext redirectTo: self application url.
	self unregister.
]
