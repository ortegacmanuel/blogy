Class {
	#name : #BYPostsListComponent,
	#superclass : #BYScreenComponent,
	#instVars : [
		'postComponents',
		'currentCategory',
		'showLoginError'
	],
	#category : #'Blogy-Components'
}

{ #category : #'as yet unclassified' }
BYPostsListComponent >> basicRenderCategoriesOn: html [

	html render: self categoriesComponent
]

{ #category : #'as yet unclassified' }
BYPostsListComponent >> basicRenderPostsOn: html [

	self postComponents do: [ :p |
	html render: (self postComponentFor: p) ]
]

{ #category : #'as yet unclassified' }
BYPostsListComponent >> categoriesComponent [

	^ BYCategoriesComponent
		categories: self blog allCategories
		postsList: self
]

{ #category : #initialization }
BYPostsListComponent >> children [

	^ self postComponents, super children
]

{ #category : #accessing }
BYPostsListComponent >> currentCategory [

	^ currentCategory
]

{ #category : #accessing }
BYPostsListComponent >> currentCategory: anObject [

	currentCategory := anObject
]

{ #category : #'as yet unclassified' }
BYPostsListComponent >> goToAdministrationView [

	self call: BYAdminComponent new
]

{ #category : #initialization }
BYPostsListComponent >> hasLoginError [

	^ showLoginError ifNil: [ false ]
]

{ #category : #initialization }
BYPostsListComponent >> initialize [

	super initialize.
	postComponents := OrderedCollection new
]

{ #category : #initialization }
BYPostsListComponent >> loginErrorMessage [
	
	^ 'Incorrect login and/or password'
]

{ #category : #initialization }
BYPostsListComponent >> loginErrorOccurred [

	showLoginError := true
]

{ #category : #'as yet unclassified' }
BYPostsListComponent >> postComponentFor: aPost [
	
	^ BYPostComponent new post: aPost
]

{ #category : #initialization }
BYPostsListComponent >> postComponents [

	postComponents := self readSelectedPosts
	collect: [ :each | self postComponentFor: each ].
	^ postComponents
]

{ #category : #'as yet unclassified' }
BYPostsListComponent >> readSelectedPosts [

	^ self currentCategory
		ifNil: [ self blog allVisibleBlogPosts ]
		ifNotNil: [ self blog allVisibleBlogPostsFromCategory: self
			currentCategory ]
]

{ #category : #'as yet unclassified' }
BYPostsListComponent >> renderCategoryColumnOn: html [

	html column
		extraSmallSize: 12;
		smallSize: 2;
		mediumSize: 4;
		with: [ self basicRenderCategoriesOn: html ]
]

{ #category : #rendering }
BYPostsListComponent >> renderContentOn: html [

	super renderContentOn: html.
	html container: [
	html row
		with: [ self renderCategoryColumnOn: html.
			self renderPostColumnOn: html ] ].
	
	"Add the necessary JS scripts - use the library (SBSFileLibrary subclass) that fits best your deployment purposes"
	SBSDeploymentLibrary addLoadScriptTo: html
]

{ #category : #'as yet unclassified' }
BYPostsListComponent >> renderLoginErrorMessageIfAnyOn: html [

	self hasLoginError ifTrue: [
		showLoginError := false.
		html alert
			beDanger ;
			with: self loginErrorMessage
	]
]

{ #category : #'as yet unclassified' }
BYPostsListComponent >> renderPostColumnOn: html [


	html column
		extraSmallSize: 12;
		smallSize: 10;
		mediumSize: 8;
		with: [ 
			self renderLoginErrorMessageIfAnyOn: html.
			self basicRenderPostsOn: html ]
]

{ #category : #'as yet unclassified' }
BYPostsListComponent >> tryConnectionWithLogin: login andPassword: password [
		
	(login = self blog administrator login and: [
		(SHA256 hashMessage: password) = self blog administrator password ])
			ifTrue: [ 
				self session currentAdmin: self blog administrator.
				self goToAdministrationView ]
			ifFalse: [ self loginErrorOccurred ]
]
