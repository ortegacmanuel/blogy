Class {
	#name : #BYPostComponent,
	#superclass : #WAComponent,
	#instVars : [
		'post'
	],
	#category : #'Blogy-Components'
}

{ #category : #accessing }
BYPostComponent >> date [

	^ post date
]

{ #category : #initialization }
BYPostComponent >> initialize [

	super initialize.
	post := BYPost new
]

{ #category : #accessing }
BYPostComponent >> post: aPost [

	post := aPost
]

{ #category : #accessing }
BYPostComponent >> renderContentOn: html [

	html heading level: 2; with: self title.
	html heading level: 6; with: self date.
	html text: self text
]

{ #category : #accessing }
BYPostComponent >> text [

	^ post text
]

{ #category : #accessing }
BYPostComponent >> title [

	^ post title
]
