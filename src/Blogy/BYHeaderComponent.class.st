Class {
	#name : #BYHeaderComponent,
	#superclass : #WAComponent,
	#instVars : [
		'component'
	],
	#category : #'Blogy-Components'
}

{ #category : #'instance creation' }
BYHeaderComponent class >> from: aComponent [

	^ self new
		component: aComponent;
		yourself
]

{ #category : #accessing }
BYHeaderComponent >> component [

	^ component
]

{ #category : #accessing }
BYHeaderComponent >> component: anObject [

	component := anObject
]

{ #category : #rendering }
BYHeaderComponent >> renderContentOn: html [
	
	| bar id|
	id := '#navbarCollapsed'.
	bar := html navigationBar.
	bar beLight; expandLarge.
	bar background beLight.
	bar with: [ 
		html navigationBarBrand with: [
			html navigationLink 
				url: self application url;
				with: 'Blogy'
		].
		
		"The toggler that is only visible when reducing the width of screen"
		html
			navigationBarToggler
			beButtonType;
			collapse;
			dataTarget: id;
			with: [ html navigationBarTogglerIcon ].
				
		html navigationBarCollapse collapse id: 'navbarCollapsed'; with: [ 
			html navigationBarNavigation: [
				html navigationItem beActive with: [ html navigationLink: 'Home' ].
				html navigationItem: [ html navigationLink: 'Link' ].
				self renderToogleViewLinkOn: html.
				self session isLogged
					ifTrue: [  self renderDisconnectButtonOn: html ]
					ifFalse: [ self renderModalLoginButtonOn: html ]
			]
		]
	]
]

{ #category : #rendering }
BYHeaderComponent >> renderDisconnectButtonOn: html [
	
	html form: [ 
		html formButton
			beSecondary
			callback: [ self session reset ];
			with: 'Logout' ]
]

{ #category : #rendering }
BYHeaderComponent >> renderModalLoginButtonOn: html [

	html formButton
		bePrimary;
		dataToggle: 'modal';
		dataTarget: '#myAuthDialog';
		with: 'Login'.
	html render: (BYAuthentificationComponent from: component).
]

{ #category : #rendering }
BYHeaderComponent >> renderToogleViewLinkOn: html [

	html navigationItem: [ 
		html navigationLink
			callback: [ component goToAdministrationView ];
			with: 'Admin View' ].
]

{ #category : #rendering }
BYHeaderComponent >> rendererClass [
	
	^ SBSHtmlCanvas 
]
