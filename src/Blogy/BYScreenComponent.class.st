Class {
	#name : #BYScreenComponent,
	#superclass : #WAComponent,
	#instVars : [
		'header'
	],
	#category : #'Blogy-Components'
}

{ #category : #'as yet unclassified' }
BYScreenComponent >> blog [

	^ BYBlog current
]

{ #category : #accessing }
BYScreenComponent >> children [

	^ { header }	
]

{ #category : #initialization }
BYScreenComponent >> createHeaderComponent [

	^ BYHeaderComponent from: self
]

{ #category : #initialization }
BYScreenComponent >> initialize [

	super initialize.
	header := self createHeaderComponent
]

{ #category : #rendering }
BYScreenComponent >> renderContentOn: html [

	html render: header
]

{ #category : #initialization }
BYScreenComponent >> rendererClass [
	
	^ SBSHtmlCanvas 
]
