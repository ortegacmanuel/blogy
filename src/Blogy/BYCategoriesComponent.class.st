Class {
	#name : #BYCategoriesComponent,
	#superclass : #WAComponent,
	#instVars : [
		'categories',
		'postsList'
	],
	#category : #'Blogy-Components'
}

{ #category : #'as yet unclassified' }
BYCategoriesComponent class >> categories: categories postsList: aBYScreen [

	^ self new categories: categories; postsList: aBYScreen
]

{ #category : #accessing }
BYCategoriesComponent >> categories [

	^ categories
]

{ #category : #accessing }
BYCategoriesComponent >> categories: aCollection [

	categories := aCollection asSortedCollection 
]

{ #category : #accessing }
BYCategoriesComponent >> postsList [

	^ postsList
]

{ #category : #accessing }
BYCategoriesComponent >> postsList: aComponent [

	postsList := aComponent
]

{ #category : #rendering }
BYCategoriesComponent >> renderCategoryLinkOn: html with: aCategory [

	html listGroupLinkedItem
		class: 'active' if: aCategory = self postsList currentCategory;
		callback: [ self selectCategory: aCategory ];
		with: aCategory
]

{ #category : #rendering }
BYCategoriesComponent >> renderContentOn: html [

	html listGroup: [
		html listGroupItem
			with: [ html strong: 'Categories' ].
			categories do: [ :cat |
				self renderCategoryLinkOn: html with: cat ] ]
]

{ #category : #rendering }
BYCategoriesComponent >> rendererClass [
	
	^ SBSHtmlCanvas 
]

{ #category : #'as yet unclassified' }
BYCategoriesComponent >> selectCategory: aCategory [

	postsList currentCategory: aCategory
]
