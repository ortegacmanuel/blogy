Class {
	#name : #BYAdminHeaderComponent,
	#superclass : #BYHeaderComponent,
	#category : #'Blogy-Components'
}

{ #category : #rendering }
BYAdminHeaderComponent >> renderToogleViewLinkOn: html [

	html navigationItem: [ 
		html navigationLink
		callback: [ component goToPostListView ];
		with: 'Post List View' ].
]
