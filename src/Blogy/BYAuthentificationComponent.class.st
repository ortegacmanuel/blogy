Class {
	#name : #BYAuthentificationComponent,
	#superclass : #WAComponent,
	#instVars : [
		'password',
		'account',
		'component'
	],
	#category : #'Blogy-Components'
}

{ #category : #'instance creation' }
BYAuthentificationComponent class >> from: aComponent [

	^ self new
		component: aComponent;
		yourself
]

{ #category : #accessing }
BYAuthentificationComponent >> account [

	^ account
]

{ #category : #accessing }
BYAuthentificationComponent >> account: anObject [

	account := anObject
]

{ #category : #accessing }
BYAuthentificationComponent >> component [

	^ component
]

{ #category : #accessing }
BYAuthentificationComponent >> component: anObject [

	component := anObject
]

{ #category : #accessing }
BYAuthentificationComponent >> password [

	^ password
]

{ #category : #accessing }
BYAuthentificationComponent >> password: anObject [

	password := anObject
]

{ #category : #rendering }
BYAuthentificationComponent >> renderAccountFieldOn: html [

	html
		formGroup: [ html label with: 'Account'.
			html textInput
				formControl;
				attributeAt: 'autofocus' put: 'true';
				callback: [ :value | account := value ];
				value: account ]
]

{ #category : #rendering }
BYAuthentificationComponent >> renderBodyOn: html [

	html
		modalBody: [
			html form: [
				self renderAccountFieldOn: html.
				self renderPasswordFieldOn: html.
				html modalFooter: [ self renderButtonsOn: html ]
			]
		]
]

{ #category : #rendering }
BYAuthentificationComponent >> renderButtonsOn: html [

	html formButton
		attributeAt: 'type' put: 'button';
		attributeAt: 'data-dismiss' put: 'modal';
		beSecondary;
		value: 'Cancel'.
	html submitButton
		callback: [ self validate ];
		value: 'SignIn'
]

{ #category : #rendering }
BYAuthentificationComponent >> renderContentOn: html [

	html modal
		id: 'myAuthDialog';
		fade;
		with: [
			html modalDialog: [
			html modalContent: [
				self renderHeaderOn: html.
				self renderBodyOn: html ] ] ]
]

{ #category : #rendering }
BYAuthentificationComponent >> renderHeaderOn: html [

	html
		modalHeader: [
			html modalTitle
				level: 4;
				with: 'Authentication'.
			html modalCloseButton ]
]

{ #category : #rendering }
BYAuthentificationComponent >> renderPasswordFieldOn: html [

	html formGroup: [
		html label with: 'Password'.
		html passwordInput
			formControl;
			callback: [ :value | password := value ];
			value: password ]
]

{ #category : #rendering }
BYAuthentificationComponent >> rendererClass [
	
	^ SBSHtmlCanvas 
]

{ #category : #rendering }
BYAuthentificationComponent >> validate [

	^ component tryConnectionWithLogin: self account andPassword: self password
]
