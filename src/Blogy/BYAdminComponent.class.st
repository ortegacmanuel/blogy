Class {
	#name : #BYAdminComponent,
	#superclass : #BYScreenComponent,
	#instVars : [
		'report'
	],
	#category : #'Blogy-Components'
}

{ #category : #accessing }
BYAdminComponent >> children [

	^ super children copyWith: self report
]

{ #category : #initialization }
BYAdminComponent >> createHeaderComponent [

	^ BYAdminHeaderComponent from: self
]

{ #category : #'as yet unclassified' }
BYAdminComponent >> goToPostListView [

	self answer
]

{ #category : #initialization }
BYAdminComponent >> initialize [

	super initialize.
	self report: (BYPostsReport from: self blog)
]

{ #category : #rendering }
BYAdminComponent >> renderContentOn: html [

	super renderContentOn: html.
	html container: [
		html heading: 'Blog Admin'.
		html horizontalRule.
		html render: self report ].
	
	"Add the necessary JS scripts - use the library (SBSFileLibrary subclass) that fits best your deployment purposes"
	SBSDeploymentLibrary addLoadScriptTo: html
]

{ #category : #accessing }
BYAdminComponent >> report [

	^ report
]

{ #category : #accessing }
BYAdminComponent >> report: aReport [

	report := aReport 
]
