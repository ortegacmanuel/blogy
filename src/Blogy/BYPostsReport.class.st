Class {
	#name : #BYPostsReport,
	#superclass : #MAReport,
	#instVars : [
		'blog'
	],
	#category : #'Blogy-Components'
}

{ #category : #'as yet unclassified' }
BYPostsReport class >> filteredDescriptionsFrom: aBlogPost [
	"Filter only some descriptions for the report columns."
	
	^ aBlogPost magritteDescription select: [ :each | #(title category date) includes: each accessor selector ]
]

{ #category : #'instance creation' }
BYPostsReport class >> from: aBlog [

	| report allBlogs |
	allBlogs := aBlog allBlogPosts.
	report := self rows: allBlogs description: (self filteredDescriptionsFrom: allBlogs anyOne).
	
	report blog: aBlog.
	report addColumn: (MACommandColumn new
		addCommandOn: report selector: #viewPost: text: 'View';
		yourself;
		addCommandOn: report selector: #editPost: text: 'Edit';
		yourself;
		addCommandOn: report selector: #deletePost: text: 'Delete';
		yourself).
	^ report
]

{ #category : #adding }
BYPostsReport >> addPost [

	| post |
	post := self call: (self renderAddPostForm:	 BYPost new).
	post ifNotNil: [ blog writeBlogPost: post. self refreshReport ]
]

{ #category : #accessing }
BYPostsReport >> blog [

	^ blog
]

{ #category : #accessing }
BYPostsReport >> blog: aBYBlog [

	blog := aBYBlog
]

{ #category : #'as yet unclassified' }
BYPostsReport >> deletePost: aPost [

	(self confirm: 'Do you want remove this post ?')
		ifTrue: [ blog removeBlogPost: aPost. self refreshReport ]
]

{ #category : #'as yet unclassified' }
BYPostsReport >> editPost: aPost [

	| post |
	post := self call: (self renderEditPostForm: aPost).
	post ifNotNil: [ blog save ]
]

{ #category : #'as yet unclassified' }
BYPostsReport >> refreshReport [

	self rows: blog allBlogPosts.
	self refresh.
]

{ #category : #rendering }
BYPostsReport >> renderAddPostForm: aPost [

	^ aPost asComponent
		addDecoration: (MAFormDecoration buttons: { #save -> 'Add post' . #cancel -> 'Cancel'});
		yourself
]

{ #category : #rendering }
BYPostsReport >> renderContentOn: html [

	html anchor
		callback: [ self addPost ];
		with: 'Add post'.
		super renderContentOn: html
]

{ #category : #rendering }
BYPostsReport >> renderEditPostForm: aPost [

	^ aPost asComponent addDecoration: (
		MAFormDecoration buttons: {
			#save -> 'Save post'.
			#cancel -> 'Cancel'});
		yourself
]

{ #category : #rendering }
BYPostsReport >> renderViewPostForm: aPost [

	^ aPost asComponent
		addDecoration: (MAFormDecoration buttons: { #cancel -> 'Back' });
		readonly: true;
		yourself
]

{ #category : #'as yet unclassified' }
BYPostsReport >> viewPost: aPost [

	self call: (self renderViewPostForm: aPost)
]
