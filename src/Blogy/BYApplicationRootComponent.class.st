Class {
	#name : #BYApplicationRootComponent,
	#superclass : #WAComponent,
	#instVars : [
		'main'
	],
	#category : #'Blogy-Components'
}

{ #category : #initialization }
BYApplicationRootComponent class >> canBeRoot [

	^ true
]

{ #category : #'class initialization' }
BYApplicationRootComponent class >> initialize [
	
	| app |
	app := WAAdmin register: self asApplicationAt: 'Blogy'.
	app
		preferenceAt: #sessionClass put: BYSession.
	app
		addLibrary: JQDeploymentLibrary;
		addLibrary: JQUiDeploymentLibrary;
		addLibrary: SBSDeploymentLibrary

]

{ #category : #rendering }
BYApplicationRootComponent >> children [

	^ { main }
]

{ #category : #initialization }
BYApplicationRootComponent >> initialize [ 

	super initialize.
	main := BYPostsListComponent new
]

{ #category : #rendering }
BYApplicationRootComponent >> main: aComponent [
	
	main := aComponent
]

{ #category : #rendering }
BYApplicationRootComponent >> renderContentOn: html [

	html render: main
]

{ #category : #rendering }
BYApplicationRootComponent >> updateRoot: anHtmlRoot [

	super updateRoot: anHtmlRoot.
	anHtmlRoot beHtml5.
	anHtmlRoot title: 'Blogy'
]
