Class {
	#name : #BYAdministrator,
	#superclass : #Object,
	#instVars : [
		'login',
		'password'
	],
	#category : #'Blogy-Domain'
}

{ #category : #'instance creation' }
BYAdministrator class >> login: login password: password [

	^ self new
		login: login;
		password: password;
		yourself
]

{ #category : #accessing }
BYAdministrator >> login [

	^ login
]

{ #category : #accessing }
BYAdministrator >> login: anObject [

	login := anObject
]

{ #category : #accessing }
BYAdministrator >> password [

	^ password
]

{ #category : #accessing }
BYAdministrator >> password: anObject [

	password := SHA256 hashMessage: anObject
]
