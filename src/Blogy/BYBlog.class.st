Class {
	#name : #BYBlog,
	#superclass : #Object,
	#instVars : [
		'posts',
		'adminUser'
	],
	#category : #'Blogy-Domain'
}

{ #category : #accessing }
BYBlog class >> createDemoPosts [
	"TBBlog createDemoPosts"
	
	self current 	
		writeBlogPost: ((BYPost title: 'Welcome in TinyBlog' text: 'TinyBlog is a small blog engine made with Pharo.' category: 'TinyBlog') visible: true);
		writeBlogPost: ((BYPost title: 'Report Pharo Sprint' text: 'Friday, June 12 there was a Pharo sprint / Moose dojo. It was a nice event with more than 15 motivated sprinters. With the help
of candies, cakes and chocolate, huge work has been done' category: 'Pharo') visible: true);
		writeBlogPost: ((BYPost title: 'Brick on top of Bloc - Preview' text: 'We are happy to announce the first preview version of Brick, a new widget set created from scratch on top of Bloc. Brick is being developed primarily by Alex Syrel (together with Alain Plantec, Andrei Chis and myself), and the
work is sponsored by ESUG.	Brick is part of the Glamorous Toolkit effort and will provide
the basis for the new versions of the development tools.' category: 'Pharo') visible: true); 	writeBlogPost: ((BYPost title: 'The sad story of unclassified blog posts' text: 'So sad that I can read this.') visible: true);
	writeBlogPost: ((BYPost title: 'Working with Pharo on the Raspberry Pi' text: 'Hardware is getting cheaper and many new small devices like the famous Raspberry Pi provide new computation power that was one once only available on regular desktop computers.' category: 'Pharo') visible: true)	
]

{ #category : #accessing }
BYBlog class >> current [

	^ self selectAll
		ifNotEmpty: [ :x | x anyOne ]
		ifEmpty: [ self new save ]
]

{ #category : #'as yet unclassified' }
BYBlog class >> defaultAdminLogin [

	^ 'admin'
]

{ #category : #'as yet unclassified' }
BYBlog class >> defaultAdminPassword [

	^ 'topsecret'
	
]

{ #category : #initialization }
BYBlog class >> initialize [
	
	self reset
]

{ #category : #accessing }
BYBlog class >> initializeVoyageOnMemoryDB [

	VOMemoryRepository new enableSingleton
]

{ #category : #accessing }
BYBlog class >> isVoyageRoot [
	"Indicates that instances of this class are top level documents in noSQL databases"

	^ true
]

{ #category : #initialization }
BYBlog class >> reset [
	
	self initializeVoyageOnMemoryDB
]

{ #category : #'as yet unclassified' }
BYBlog >> administrator [

	^ adminUser 
]

{ #category : #accessing }
BYBlog >> allBlogPosts [
	^ posts
]

{ #category : #accessing }
BYBlog >> allBlogPostsFromCategory: aCategory [

	^ posts select: [ :p | p category = aCategory ]	
]

{ #category : #accessing }
BYBlog >> allCategories [
	
	^ (posts collect: [ :p | p category ]) asSet
]

{ #category : #'accessing - attributes' }
BYBlog >> allVisibleBlogPosts [
	
	^ posts select: [ :p | p isVisible ]
]

{ #category : #accessing }
BYBlog >> allVisibleBlogPostsFromCategory: aCategory [ 
	^ posts select: [ :p | p category = aCategory and: [ p isVisible ] ]
]

{ #category : #'as yet unclassified' }
BYBlog >> createAdministrator [

	^ BYAdministrator
		login: self class defaultAdminLogin
		password: self class defaultAdminPassword
]

{ #category : #initialization }
BYBlog >> initialize [ 
	
	super initialize.
	posts :=	OrderedCollection new.
	adminUser := self createAdministrator
]

{ #category : #operations }
BYBlog >> removeAllPosts [

	posts := OrderedCollection new.
	self save
]

{ #category : #removing }
BYBlog >> removeBlogPost: aPost [

	posts remove: aPost ifAbsent: [ ].
	self save.
]

{ #category : #accessing }
BYBlog >> size [
	^ posts size
]

{ #category : #operations }
BYBlog >> writeBlogPost: aPost [ 
	"Add blog post to the list of posts."
	
	self allBlogPosts add: aPost.
	self save 
]
